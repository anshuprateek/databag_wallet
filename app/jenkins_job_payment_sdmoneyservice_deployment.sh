export ANSIBLE_HOST_KEY_CHECKING=False
export inventory_file=ansible_inventory.txt
export deployment_user=srv_walletbot
export servicename=sdmoneyservice

echo '[all]' > ${inventory_file}
echo ${hostname_sdm} >> ${inventory_file}

export ansible_args="-i ${inventory_file} -f 1 --private-key /var/lib/jenkins/.secrets/id_rsa_wallet_bot -u ${deployment_user} -e hosts=all"

/usr/bin/ansible-playbook sdm_install.yml ${ansible_args}
/usr/bin/ansible-playbook ../../payment_sdmoneyservice_secrets/workspace/sdm_install_secrets_all.yml --vault-password-file ~/.secrets/vault_pass.txt ${ansible_args}


if [[ $hostname_sdm == *"getb"* ]]
    then
        /usr/bin/ansible-playbook update-databag-getb-app.yml ${ansible_args}
elif [[ $hostname_sdm == *"bg"* ]]
    then
        /usr/bin/ansible-playbook update-databag-bg-app.yml ${ansible_args}
fi
    
ansible all -s -m service -a "name=${servicename} state=restarted"  ${ansible_args}
